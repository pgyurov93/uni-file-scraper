__author__ = 'Petar'

from FileScraper import *


class FundAlg(FileScraper):

    def get_pdfs(self):
        for link in self.soup.findAll('a', href=True):
            if link['href'].endswith('pdf'):
                cont = self.existing_files(link['href'])
                if cont:
                    pdf_file_url = self.url.replace('/index.html.en', '') + link['href'][1:]  # shitty hack to get proper url

                    data = urllib2.urlopen(pdf_file_url).read()
                    file_save = self.directory + '/' + os.path.basename(pdf_file_url)
                    with open(file_save, 'wb') as my_file:
                        my_file.write(data)

                else:
                    print "File " + os.path.basename(link['href']) + " already exists."

"""
fa = FundAlg()
fa.save_in('C:/Users/Petar/Desktop/Temp/FundAlg')
fa.page_url('http://wwwmayr.in.tum.de/lehre/2015WS/fa/index.html.en')
fa.get_pdfs()
"""