__author__ = 'Petar'

import requests
from bs4 import BeautifulSoup, SoupStrainer
import urllib2
import os

import time
import datetime


class FileScraper:
    def __init__(self):
        Date = str(datetime.date.today())
        Time = str(datetime.datetime.now().time())[:8]

    def save_in(self, dir):
        self.directory = dir

        #get list of existing files for the check later
        self.filelist = [ f for f in os.listdir(self.directory) if os.path.isfile(os.path.join(self.directory,f)) ]

    def page_url(self, url):
        self.url = url
        self.r = requests.get(self.url)
        self.soup = BeautifulSoup(self.r.text)

    def get_pdfs(self):
        for link in self.soup.findAll('a', href=True):
            if link['href'].endswith('pdf'):
                cont = self.existing_files(link['href'])
                if cont:
                    data = urllib2.urlopen(link['href']).read()
                    file_save = self.directory + '/' + os.path.basename(link['href'])
                    with open(file_save, 'wb') as my_file:
                        my_file.write(data)
                else:
                    print "File " + os.path.basename(link['href']) + " already exists."

    def existing_files(self, file_link):
        filename = os.path.basename(file_link)
        if filename in self.filelist:
            cont = False
        else:
            cont = True
        return cont

    #prototype from other scraper
    def periodic_update(self, period, duration):
        check = True
        start = time.time()
        time.clock()

        while (check == True):
            self.update()
            time.sleep(period)
            if (time.time() - start > duration):
                check = False


"""
fs = FileScraper()
fs.save_in('C:/Users/Petar/Desktop/Temp')

scicomp = 'http://www5.in.tum.de/wiki/index.php/Scientific_Computing_I_-_Winter_15'
fundalg = 'http://wwwmayr.in.tum.de/lehre/2015WS/fa/index.html.en'

fs.page_url(scicomp)
fs.get_pdfs()
"""